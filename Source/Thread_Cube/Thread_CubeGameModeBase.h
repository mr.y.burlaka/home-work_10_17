// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HAL/RunnableThread.h"
#include "IMessageBus.h"
#include "SimpleAtomic_Runnable.h"
#include "SimpleCounter_Runnable.h"
#include "SimpleMutex_Runnable.h"
#include "SimpleCollectable_Runnable.h"
#include "Thread_CubeGameModeBase.generated.h"

USTRUCT(BlueprintType, Atomic)
struct FInfoNPC
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Id = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Name = "none";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString SecondName = "none";
};

struct my_struct
{
	int32 Id;
	char Name;
};

USTRUCT()
struct FBusStructMessage_NameGenerator
{
	GENERATED_BODY()

		bool bIsSecondName = false;
	FString TextName = "None";
	FBusStructMessage_NameGenerator(bool InBool = false, FString InText = "None") : bIsSecondName(InBool), TextName(InText) {}

};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateByNameGeneratorThreads, bool, bIsSecond, FString, StringData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByThreadNPC, FInfoNPC, NPCData);

/**
 *
 */

UCLASS()
class THREAD_CUBE_API AThread_CubeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintAssignable)
		FOnUpdateByNameGeneratorThreads OnUpdateByNameGeneratorThreads;
	UPROPERTY(BlueprintAssignable)
		FOnUpdateByThreadNPC OnUpdateByThreadNPC;

	//MessageBus Setting start
	void BusMessageHandler(const struct FBusStructMessage_NameGenerator& Message,
		const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandlerNPCInfo(const struct FInfoNPC& Message,
		const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndPoint_NameGenerator;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_NPCInfo;

	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay()override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason)override;

	void PrintMessage(FString message);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ThreadExampleGameModeBase setting")
		bool ShowDebugAllThread = false;

	//SimpleCounter Setting
	UPROPERTY(BlueprintReadWrite, Category = "SimpleCounter setting")
		bool bIsUseSafeVariable = true;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleCounter setting")
		FColor ColorThread;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleCounter setting")
		bool bIsUseFEvent = true;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleCounter setting")
		bool bIsUseFSScopedEvent = true;

	class FSimpleCounter_Runnable *MyRunnableClass_SimpleCounter = nullptr;
	FRunnableThread* CurrentRunningGameModeThread_SimpleCounter = nullptr;
	FEvent *SimpleCounterEvent;
	FScopedEvent* SimpleCounterScopedEvent_Ref;

	//SimpleCounter Control
	UFUNCTION(BlueprintCallable)
		void StopSimpleCounterThread();
	UFUNCTION(BlueprintCallable)
		void KillSimpleCounterThread(bool bIsShouldWait);
	UFUNCTION(BlueprintCallable)
		bool SwitchRunStateSimpleCounterThread(bool bIsPause);
	UFUNCTION(BlueprintCallable)
		void CreateSimpleCounterThread();
	UFUNCTION(BlueprintCallable)
		void StartSimpleCounterThreadWithEvent();
	UFUNCTION(BlueprintCallable)
		void StartSimpleCounterThreadWithScopedEvent();
	UFUNCTION(BlueprintCallable)
		int64 GetCounterSimpleCounterThread();

	void SendRef_ScopedEvent(FScopedEvent& ScopedEvent_Ref);

	// SimpleAtomic Setting
	TArray<FRunnableThread*> CurrentRunnbleGameModeThread_SimpleAtomic;

	UPROPERTY(BlueprintReadWrite,Category="SimpleAtomic Setting")
		int32 IterationRunnableCircle = 100000;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleAtomic Setting")
		int32 NumberOfThreadToCreate = 12;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleAtomic Setting")
		bool bIsUseAtomic = true;

	// SimpleAtomic Control
	UFUNCTION(BlueprintCallable)
		void CreateSimpleAtomicThread();
	UFUNCTION(BlueprintCallable)
		void GetCounterSimpleAtomic(int32& Atomic1, int32& Atomic2, int32& NotAtomic1, int32& NotAtomic2);
	UFUNCTION(BlueprintCallable)
		void ResetCounterSimpleAtomic();
	// SimpleAtomic Storage
	std::atomic_int16_t AtomicCounter1;
	std::atomic_int16_t AtomicCounter2;
	int16 NotAtomicCounter1;
	int16 NotAtomicCounter2;

	//SimpleMutex Setting
	TArray<FRunnableThread*> CurrentRunningGameModeThread_SimpleMutex;
	FRunnableThread* CurrentRunningGameModeThread_SimpleCollectable;

	//SimpleMutex Control
	UFUNCTION(BlueprintCallable)
		void StopSimpleMutexThreads();
	UFUNCTION(BlueprintCallable)
		void CreateSimpleMutexThread();
	UFUNCTION(BlueprintCallable)
		void CreateCollectableThread();
	TArray<FInfoNPC> GetNPCInfo();
	UFUNCTION(BlueprintCallable)
		TArray<FString> GetSecondNames();
	UFUNCTION(BlueprintCallable)
		TArray<FString> GetFirstNames();
	UFUNCTION(BlueprintCallable)
		void Clear();

	void EventMessage(bool bIsSecond, FString StringData);
	void EventMessageNPC(FInfoNPC NPCData);

	//SimpleMutex storage
	TArray<FString> FirstNames;

	FCriticalSection FirstNameMutex;
	TQueue<FString, EQueueMode::Mpsc> SecondNames;

	FCriticalSection NPCInfoMutex;
	TArray<FInfoNPC> NPCInfo;

	TArray<FString> CurrentSecondName;
	//TLockFreePointerList //Same TQueue (TQueue under the hood linked list(LockFreePointerList))
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SimpleMutex setting")
		TSubclassOf<class ADumbCuteCube> SpawnObjectThread;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SimpleMutex setting")
		FVector SpawnCuteCubeLoc;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SimpleMutex setting")
		FVector SpawnCuteCubeLoc_Step;
	int32 cubeCout = 0;


	//ParallelFor
	UFUNCTION(BlueprintCallable)
		void StartParallelFor1();
	UPROPERTY(BlueprintReadWrite)
		int32 ParallelCout1 = 0;
	UFUNCTION(BlueprintCallable)
		void StartParallelFor2();
	UPROPERTY(BlueprintReadWrite)
		int32 ParallelCout2 = 0;
	UFUNCTION(BlueprintCallable)
		void StartParallelFor3();
	UPROPERTY(BlueprintReadWrite)
		int32 ParallelCout3 = 0;
};
