// Fill out your copyright notice in the Description page of Project Settings.


#include "DumbCuteCube.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine.h"
#include"Kismet/KismetMathLibrary.h"

// Sets default values
ADumbCuteCube::ADumbCuteCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	static ConstructorHelpers::FObjectFinder<UMaterial>MAT(TEXT("Material'/Game/MyRandomMaterrialColor.MyRandomMaterrialColor'"));

	if (MAT.Object != NULL)
	{
		m_Dynamic = (UMaterial*)MAT.Object;
	}
}

// Called when the game starts or when spawned
void ADumbCuteCube::BeginPlay()
{
	Super::BeginPlay();
	if (m_Dynamic)
	{
		MaterialInstance = UMaterialInstanceDynamic::Create(m_Dynamic, this);
	}
	if (MaterialInstance)
	{
		MeshComponent->SetMaterial(0, MaterialInstance);
	}
}

// Called every frame
void ADumbCuteCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADumbCuteCube::Init(FInfoNPC InitInfo)
{
	InitBP(InitInfo);
}

void ADumbCuteCube::InitBP_Implementation(FInfoNPC InitInfo)
{
	//BP

}

void ADumbCuteCube::ChengeColor()
{
	MaterialInstance->SetVectorParameterValue("ColorRandom", FLinearColor(UKismetMathLibrary::RandomFloatInRange(0.0f, 1.0f), UKismetMathLibrary::RandomFloatInRange(0.0f, 1.0f), UKismetMathLibrary::RandomFloatInRange(0.0f, 1.0f)));
}