// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HAL/ThreadManager.h"

class AThread_CubeGameModeBase;
/**
 * 
 */
class THREAD_CUBE_API FSimpleAtomic_Runnable :public FRunnable
{
public:
	FSimpleAtomic_Runnable(FColor Color,AThread_CubeGameModeBase *OwnerActor, uint32 NeedIteration, bool SeparateLogic, bool bIsUseAtomic);
	virtual ~FSimpleAtomic_Runnable() override;

	//setting
	FColor ThreadColor;
	int NumberIteration = 0;
	AThread_CubeGameModeBase *GameMode_Ref = nullptr;
	bool bIsStopThread = false;
	bool bIsUseAtomicFlag = true;
	bool SeparateLogicFlag = false;


	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;
};
