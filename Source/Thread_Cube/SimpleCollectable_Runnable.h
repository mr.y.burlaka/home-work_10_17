// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MessageEndpoint.h"

class AThread_CubeGameModeBase;
/**
 * 
 */
class THREAD_CUBE_API FSimpleCollectable_Runnable :public FRunnable
{
public:
	FSimpleCollectable_Runnable(FColor Color, AThread_CubeGameModeBase* OwnerActor);
	virtual ~FSimpleCollectable_Runnable() override;

	//setting
	AThread_CubeGameModeBase* GameMode_Ref = nullptr;
	FThreadSafeBool bIsStopCollectable = false;

	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	//IMessageBus Setting
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndpoint;
};
