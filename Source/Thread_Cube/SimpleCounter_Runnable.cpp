// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleCounter_Runnable.h"
#include <thread>
#include <thread>//old
#include "Thread_CubeGameModeBase.h"

FSimpleCounter_Runnable::FSimpleCounter_Runnable(FColor Color, AThread_CubeGameModeBase* OwnerActor, bool VariableMode)
{
	GameMode_Ref = OwnerActor;
	ThreadColor = Color;
	bIsUseSafeVariable = VariableMode;
	UE_LOG(LogTemp, Warning, TEXT("FNameGeneratorLogic_Runnable::Constructor"))
}

FSimpleCounter_Runnable::~FSimpleCounter_Runnable()
{
	UE_LOG(LogTemp, Warning, TEXT("FNameGeneratorLogic_Runnable::Destructor "));
}

bool FSimpleCounter_Runnable::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("FNameGeneratorLogic_Runnable::Init "));
	return true;
}
//#pragma optimize ("", off)
uint32 FSimpleCounter_Runnable::Run()
{
	if (GameMode_Ref->bIsUseFSScopedEvent)
	{
		{
			FScopedEvent SimpleCounterScopedEvent;

			GameMode_Ref->SendRef_ScopedEvent(SimpleCounterScopedEvent);
		}
	}

	//FEvent
	if (GameMode_Ref->SimpleCounterEvent)
	{
		GameMode_Ref->SimpleCounterEvent->Wait(10000);
		if (GameMode_Ref->SimpleCounterEvent) 
		{
			GameMode_Ref->SimpleCounterEvent->Wait(10000); 
		}
	}

	if (bIsUseSafeVariable)
	{
		while (!bIsStopThreadSafe)
		{
			CounterSafe.Increment();
		}
	}
	else
	{
		while (!bIsStopThread)
		{
			Counter++;
			FPlatformProcess::Sleep(0.0200f);//for chance exit loop we can add sleep
		}
	}

	return 0;
}
//#pragma optimize ("", on)
void FSimpleCounter_Runnable::Stop()
{
	UE_LOG(LogTemp, Warning, TEXT("FNameGeneratorLogic_Runnable::Stop "));
	bIsStopThread = true;
	bIsStopThreadSafe = true;
	GameMode_Ref->SimpleCounterScopedEvent_Ref = nullptr;
}

void FSimpleCounter_Runnable::Exit()
{
	GameMode_Ref->SimpleCounterScopedEvent_Ref = nullptr;
	GameMode_Ref = nullptr;
	UE_LOG(LogTemp, Warning, TEXT("FNameGeneratorLogic_Runnable::Exit "));
}
